package case2;

import java.util.stream.Stream;
import java.util.HashMap;
import java.util.Map;
import com.google.gson.Gson;
import case2.Zipcode;

import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class App {
  public static void main(String[] args) {
    Gson gson = new Gson();
    String data = "";
    try {
      String url = "https://api.zipaddress.net/?zipcode=192-0003";
      String charset = "UTF-8";
      List<String> contents = read(url, charset);
      for (String str : contents) {
        data += str;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
System.out.println(data);
Zipcode res = gson.fromJson(data, Zipcode.class);//koko
String json = gson.toJson(res);

System.out.println(json);

  }

  public static List<String> read(String url, String charset) throws Exception {
    InputStream is = null;
    InputStreamReader isr = null;
    BufferedReader br = null;
    try {
      URLConnection conn = new URL(url).openConnection();
      is = conn.getInputStream();
      isr = new InputStreamReader(is, charset);
      br = new BufferedReader(isr);

      ArrayList<String> lineList = new ArrayList<String>();
      String line = null;
      while ((line = br.readLine()) != null) {
        lineList.add(line);
      }
      return lineList;
    } finally {
      try {
        br.close();
      } catch (Exception e) {
      }
      try {
        isr.close();
      } catch (Exception e) {
      }
      try {
        is.close();
      } catch (Exception e) {
      }
    }
  }

}
